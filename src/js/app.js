import noUiSlider from 'nouislider';

const BASE_PRICES = {
  TARIFS_BASE: [6500, 10500, 36000],
  HELICOPTER: 44000,
  BEARS: 42000,
  FLIGHT_COST: 29000,
  INSURANCE: 59,
};

let state = {
  persons: 2,
  days: 8,
  tarif: 2,
  helicopter: false,
  bears: false,
  insurance: false,
  currentStep: 0,
  begun: false,
};


const $findKamchatka = document.querySelectorAll('.clcBlock_find_item_radio');
const $findKamchatkaAnswers = document.querySelectorAll('.clcBlock_find_answer_item');
const $totalPrice = document.querySelector('.clcBlock_result_value');
const $personsCount = document.querySelector('.clcBlock_result_personsCount');
const $radioItems = document.querySelectorAll('.clcBlock_checkItem_radio');
const $checkItems = document.querySelectorAll('.clcBlock_checkItem');
const $resultsItem = document.querySelector('.clcBlock_result');
const $detailsItem = document.querySelector('.clcBlock_details');
const $flyingItem = $detailsItem.querySelector('#flying');
const $tarifsItem = $detailsItem.querySelector('#tarifs');
const $addictionItem = $detailsItem.querySelector('#addiction');
const $insuranceItem = $detailsItem.querySelector('#insurance');
const $begunItems = document.querySelectorAll('.not-begun');

for(let i = 0; i<$findKamchatka.length; i++){
  $findKamchatka[i].addEventListener('change', ()=>{
    for(let j=0; j<$findKamchatkaAnswers.length; j++){
      $findKamchatkaAnswers[j].style.display = 'none'
    }
    $findKamchatkaAnswers[i].style.display = 'block';

    if(!state.begun){
      state.begun = true;

      for(let j=0; j<$begunItems.length; j++){
        $begunItems[j].classList.remove('hidden')
      }

    }



  })
}

for(let i = 0; i<$radioItems.length; i++){
  $radioItems[i].addEventListener('change', ()=>{
    state[$radioItems[i].getAttribute('name')] = JSON.parse($radioItems[i].value);
    if($radioItems[i].getAttribute('name')==='tarif' && JSON.parse($radioItems[i].value)===2){
      state.helicopter = state.bears = true;
      state.currentStep = $checkItems.length - 1;

      for(let j=0; j<$radioItems.length; j++){
        let name = $radioItems[j].getAttribute('name');
        let val = JSON.parse($radioItems[j].value);
        if((name==='helicopter'||name==='bears')&&val) $radioItems[j].checked = true;
      }
    }
    else {
      let ind = Array.prototype.indexOf.call($checkItems, $radioItems[i].parentNode.parentNode);

      if (ind>2&&ind<$checkItems.length) {
        if(state.currentStep<=ind){
          state.currentStep = ind+1;
        }
      }
      else {
        if(state.currentStep<=ind){
          state.currentStep = 3;
        }
      }



    }

    updateLoop();

  })
}

const $peoplesCount = document.getElementById('range-peoples');

noUiSlider.create($peoplesCount, {
  start: state.persons,
  range: {
    'min': [1, 1],
    '11.1111%': [2, 1],
    '22.2222%': [3, 1],
    '33.3333%': [4, 1],
    '44.4444%': [5, 1],
    '55.5555%': [6, 1],
    '66.6666%': [7, 1],
    '77.7777%': [8, 1],
    '88.8888%': [9, 1],
    'max': [10],
  },
  step: 1,
  pips: {
    mode: 'positions',
    values: [0, 11.1111, 22.22222, 33.3333, 44.4444, 55.5555, 66.6666, 77.7777, 88.8888, 99.9999],
    density: 10
  }
});

$peoplesCount.noUiSlider.on('change', (values, handle) => {
  state.persons = parseInt(values[handle]);
  updateLoop();
});

$peoplesCount.noUiSlider.on('update', (values, handle) => {

  $peoplesCount.querySelector('.noUi-touch-area').innerText = parseInt(values[handle])

});

const $daysCount = document.getElementById('range-days');

noUiSlider.create($daysCount, {
  start: state.days,
  range: {
    'min': [7, 1],
    '11.1111%': [8, 1],
    '22.2222%': [9, 1],
    '33.3333%': [10, 1],
    '44.4444%': [11, 1],
    '55.5555%': [12, 1],
    '66.6666%': [13, 1],
    '77.7777%': [14, 1],
    '88.8888%': [15, 1],
    'max': [16],
  },
  step: 1,
  pips: {
    mode: 'positions',
    values: [0, 11.1111, 22.22222, 33.3333, 44.4444, 55.5555, 66.6666, 77.7777, 88.8888, 100],
    density: 11.1111
  }
});

$daysCount.noUiSlider.on('change', (values, handle) => {
  state.days = parseInt(values[handle]);
  updateLoop();
});

$daysCount.noUiSlider.on('update', (values, handle) => {

  $daysCount.querySelector('.noUi-touch-area').innerText = parseInt(values[handle])

});

const countGetters = {
  getTarifVal: ()=> BASE_PRICES.TARIFS_BASE[state.tarif]*state.persons*state.days,
  getHelicopterPriceVal: ()=> {
    if(state.tarif===2 || state.helicopter) return state.persons*BASE_PRICES.HELICOPTER;
    return 0;
  },
  getBearsPriceVal: ()=> {
    if(state.tarif===2 || state.bears) return state.persons*BASE_PRICES.BEARS;
    return 0;
  },
  getFlightPrice: ()=> BASE_PRICES.FLIGHT_COST*state.persons,
  getInsurancesPrice: ()=> state.insurance?BASE_PRICES.INSURANCE*state.persons*state.days:0,
  getTotal: ()=> countGetters.getTarifVal()+countGetters.getHelicopterPriceVal()+countGetters.getBearsPriceVal()+countGetters.getFlightPrice()+countGetters.getInsurancesPrice()
};

const countSetters = {
  setTotalPrice: ()=>{
    $totalPrice.innerText = (countGetters.getTotal() + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' р';
    $personsCount.innerText = `${state.persons}${state.persons<2?'-го':'-х'} человек${state.persons<2?'a':''}`;
  },
  setDetails: ()=>{
    $flyingItem.innerText = (countGetters.getFlightPrice() + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' р';
    $tarifsItem.innerText = (countGetters.getTarifVal() + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' р';
    $addictionItem.innerText = (countGetters.getHelicopterPriceVal()+countGetters.getBearsPriceVal()+'').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' р';
    $insuranceItem.innerText = (countGetters.getInsurancesPrice() + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' р';
  },
  setNextStepAvailable: ()=>{
    if(state.currentStep<$checkItems.length) {
      if($checkItems[state.currentStep].classList.contains('hidden')){
        $checkItems[state.currentStep].classList.remove('hidden');
        $checkItems[state.currentStep].previousElementSibling.classList.remove('hidden');
      }
    }
  },
  toggleAvailableOptionsOfTarif: ()=>{
    if(state.tarif===2){
      for(let i=3; i<$checkItems.length-1;i++){
        $checkItems[i].classList.add('hidden');
        $checkItems[i].previousElementSibling.classList.add('hidden');
      }
    }
    else if(state.tarif<2&&state.currentStep>2){
      for(let i = 3; i<=state.currentStep; i++){
        if(i<$checkItems.length){
          $checkItems[i].classList.remove('hidden');
          $checkItems[i].previousElementSibling.classList.remove('hidden');
        }
      }
    }
  },
  showOrNotResults: ()=>{
    if(state.currentStep===$checkItems.length-1){
      $resultsItem.classList.remove('hidden');
      $detailsItem.classList.remove('hidden');
    }
  }
}

let updateLoop = ()=>{

  let keys = Object.keys(countSetters);

  for(let i=0; i<keys.length; i++){
    countSetters[keys[i]].call(this);
  }

}

updateLoop();


